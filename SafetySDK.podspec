#
# Be sure to run `pod lib lint SafetySDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SafetySDK'
  s.version          = '0.6.3'
  s.summary          = 'BuckleUp.io IOS SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
We want to make public transport stress-free and safe for everyone. Our objective is to transfer a part of our Bluetooth nearby technology, to protect anyone during their offline commute! That’s why we’re looking to integrate our Safety SDK with third-party apps. Imagine if you didn’t need to download BuckleUp App, if it was just a toggle within Google Maps, City Mapper, Candy Crush… We’re also talking to major transport providers about how they can improve public transport with design-led thinking and smart data.
                       DESC

  s.homepage        = 'https://bitbucket.org/nikiizvorski/safetysdk'
#  s.homepage         = 'https://github.com/nikiizvorski/SafetySDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Apache 2.0', :file => 'LICENSE' }
  s.author           = { 'niki.izvorski@buckleup.io' => 'niki.izvorski@buckleup.io' }
  s.source           = { :git => 'https://nikiizvorski@bitbucket.org/nikiizvorski/safetysdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'SafetySDK/Classes/*.{h,m,swift}'
  
  s.frameworks = 'CoreBluetooth','LocalAuthentication'
  s.vendored_frameworks = 'Safety.framework', 'GoogleToolboxForMac.framework'
  s.static_framework = true
  
  s.library = 'sqlite3','c++'
  
  # Specify the frameworks we are providing.
  # The app using this Pod should _not_ link these Frameworks,
  # they are bundled as a part of this Pod for technical reasons.
#  s.vendored_frameworks = 'NearbyConnections.framework'

  # LDFLAGS required by Firebase dependencies.
  s.pod_target_xcconfig = {
      'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks',
      'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  }
  
  # s.resource_bundles = {
  #   'KokoSDK' => ['KokoSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
#  s.dependency 'NearbyConnections', '~> 0.0.9'

#s.dependency 'GoogleToolboxForMac/Logger', '~> 2.1'
#  s.dependency 'NearbyConnections', '~> 0.0.9'
end
