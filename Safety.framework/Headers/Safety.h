//
//  SafetySDK.h
//
//  Created by Niki Izvorski on 08/08/2018.
//  Copyright © 2018 Niki Izvorski. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * The constant SAFETY_ANNOUNCEMENT_TERROR.
 */
static int const SAFETY_ANNOUNCEMENT_TERROR = 0;
/**
 * The constant SAFETY_ANNOUNCEMENT_DANGER.
 */
static int const SAFETY_ANNOUNCEMENT_DANGER = 1;
/**
 * The constant SAFETY_ANNOUNCEMENT_WARNING.
 */
static int const SAFETY_ANNOUNCEMENT_WARNING = 2;

@interface Safety : NSObject

/*
 Initialize Base Check
 */
- (void) initialize:(NSString*)appSecret;

/*
    Callback for Safety Announcement
 */
typedef void (^onSafetyCallback)(NSString *text);

/*
    Start Safety Scanning
 */
- (void) startSafetyScan: (onSafetyCallback) callback;

/*
    Start Safety Announcement
 */
- (void) startSafetyAnnouncement:(NSString*)text var2:(NSInteger)safetyType;

/*
 Allow Safety Mesh
 */
- (void)setAllowMesh:(bool *)allowMesh;

@end
