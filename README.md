# SafetySDK

[![CI Status](https://img.shields.io/travis/nikiizvorski@gmail.com/SafetySDK.svg?style=flat)](https://travis-ci.org/nikiizvorski@gmail.com/SafetySDK)
[![Version](https://img.shields.io/cocoapods/v/SafetySDK.svg?style=flat)](https://cocoapods.org/pods/SafetySDK)
[![License](https://img.shields.io/cocoapods/l/SafetySDK.svg?style=flat)](https://cocoapods.org/pods/SafetySDK)
[![Platform](https://img.shields.io/cocoapods/p/SafetySDK.svg?style=flat)](https://cocoapods.org/pods/SafetySDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SafetySDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SafetySDK'
```

## Author

nikiizvorski@gmail.com, dev@airdates.co

## License

SafetySDK is available under the MIT license. See the LICENSE file for more info.
